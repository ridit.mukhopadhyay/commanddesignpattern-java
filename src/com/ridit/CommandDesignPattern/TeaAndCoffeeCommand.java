package com.ridit.CommandDesignPattern;

public class TeaAndCoffeeCommand implements Command{
	
	private Chef chef;
	private Order order;
	
	public TeaAndCoffeeCommand(Chef chef,Order order) {
		// TODO Auto-generated constructor stub
		
		this.chef = chef;
		this.order = order;
		
	}

	@Override
	public void execute() {
		// TODO Auto-generated method stub
		chef.prepareTeaAndCoffee(order);
		
		
	}

	public Chef getChef() {
		return chef;
	}

	public void setChef(Chef chef) {
		this.chef = chef;
	}

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

}
