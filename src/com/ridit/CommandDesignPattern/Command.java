package com.ridit.CommandDesignPattern;

public interface Command {
	
	void execute ();

}
