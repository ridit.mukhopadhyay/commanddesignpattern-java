package com.ridit.CommandDesignPattern;

public class ItemOrder {
	
	private Item item;
	private int numberOfPlates;
	
	public ItemOrder(Item item,int numberOfPlates) {
		this.item = item;
		this.numberOfPlates = numberOfPlates;
	}
	public Item getItem() {
		return item;
	}
	public void setItem(Item item) {
		this.item = item;
	}
	public int getNumberOfPlates() {
		return numberOfPlates;
	}
	public void setNumberOfPlates(int numberOfPlates) {
		this.numberOfPlates = numberOfPlates;
	}

}
