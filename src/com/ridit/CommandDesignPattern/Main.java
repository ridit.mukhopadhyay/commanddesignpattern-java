package com.ridit.CommandDesignPattern;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Chef chef = new Chef();
		
		
		Order prepareFoodOrder = new Order();
		
		prepareFoodOrder.getItemOrders().add(new ItemOrder(new Item(1,"Bread", 100), 5));
		prepareFoodOrder.getItemOrders().add(new ItemOrder(new Item(2, "chocolate", 200), 10));
		
		Order prepareTeaAndCoffeeOrder = new Order();
		
		prepareTeaAndCoffeeOrder.getItemOrders().add(new ItemOrder(new Item(10, "Tea", 20), 5));
		prepareTeaAndCoffeeOrder.getItemOrders().add(new ItemOrder(new Item(11, "Coffee", 50), 10));
		
		PrepareFoodCommand foodCommand = new PrepareFoodCommand(chef, prepareFoodOrder);
		TeaAndCoffeeCommand teaAndCoffeComand = new TeaAndCoffeeCommand(chef, prepareTeaAndCoffeeOrder);
		
		Waiter waiterForFood = new Waiter(foodCommand);
		Waiter waiterForTeaAndCoffee = new Waiter(teaAndCoffeComand);
		
		waiterForFood.askChefToExecuteCommand();
		waiterForTeaAndCoffee.askChefToExecuteCommand();
		

	}

}
