package com.ridit.CommandDesignPattern;

import java.util.ArrayList;
import java.util.List;

public class Order {
	
	private List<ItemOrder> itemOrders = new ArrayList<>();

	public List<ItemOrder> getItemOrders() {
		return itemOrders;
	}

	public void setItemOrders(List<ItemOrder> itemOrders) {
		this.itemOrders = itemOrders;
	}

}
