package com.ridit.CommandDesignPattern;

public class PrepareFoodCommand implements Command {
	
	Chef chef;
	Order order;
	
	public PrepareFoodCommand(Chef chef, Order order) {
		// TODO Auto-generated constructor stub
		
		this.chef = chef;
		this.order = order;
	}

	@Override
	public void execute() {
		// TODO Auto-generated method stub
		
		chef.prepareFood(order);

	}

}
