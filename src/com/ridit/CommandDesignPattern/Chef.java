package com.ridit.CommandDesignPattern;

import java.util.ArrayList;


public class Chef {
	
	public void prepareTeaAndCoffee(Order order) {
		ArrayList<ItemOrder> itemOrders = (ArrayList<ItemOrder>) order.getItemOrders();
		for (int i = 0;i<itemOrders.size(); i++) {
			ItemOrder itemOrder = itemOrders.get(i);
			System.out.println(itemOrder.getNumberOfPlates() + " cups of " + itemOrder.getItem().getName() + " is getting prepared ");
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
	public void prepareFood(Order order) {
		ArrayList<ItemOrder> itemOrders = (ArrayList<ItemOrder>) order.getItemOrders();
		for (int i = 0;i<itemOrders.size(); i++) {
			ItemOrder itemOrder = itemOrders.get(i);
			System.out.println(itemOrder.getNumberOfPlates() + " plates of " + itemOrder.getItem().getName() + " is getting prepared ");
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
	}

}
